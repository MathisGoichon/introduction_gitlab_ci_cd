package com.onepoint.onetechpoint.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.onepoint.onetechpoint.model.Greeting;
import com.onepoint.onetechpoint.service.GreetingService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(GreetingController.class)
@AutoConfigureMockMvc
public class GreetingControllerIT {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    GreetingService greetingService;

    @Test
    void get_greeting_without_name_should_return_hello_world_with_id_1() throws Exception {
        // GIVEN
        Greeting expectedGreeting = new Greeting(1, "Hello World !");

        when(greetingService.getNextGreetingId()).thenReturn(1L);

        // WHEN
        ResultActions resultActions = mockMvc.perform(get("/greeting"));

        // THEN
        resultActions
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(expectedGreeting.id()))
                .andExpect(jsonPath("$.content").value(expectedGreeting.content()));
    }

    @Test
    void get_greeting_with_onepoint_name_should_return_hello_onepoint_with_id_2() throws Exception {
        // GIVEN
        Greeting expectedGreeting = new Greeting(2, "Hello Onepoint !");

        when(greetingService.getNextGreetingId()).thenReturn(2L);

        MockHttpServletRequestBuilder request =
                get("/greeting")
                        .param("name", "Onepoint");

        // WHEN
        ResultActions resultActions = mockMvc.perform(request);

        // THEN
        resultActions
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(expectedGreeting.id()))
                .andExpect(jsonPath("$.content").value(expectedGreeting.content()));
    }
}
