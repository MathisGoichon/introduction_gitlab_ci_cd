package com.onepoint.onetechpoint.controller;

import com.onepoint.onetechpoint.model.Greeting;
import com.onepoint.onetechpoint.service.GreetingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class GreetingControllerTest {

    GreetingController greetingController;

    GreetingService greetingService;

    @BeforeEach
    void setUp() {
        greetingService = mock(GreetingService.class);
        greetingController = new GreetingController(greetingService);
    }

    @Test
    void greeting_should_return_Hello_World_with_id_1() {
        // Given
        Greeting expectedGreeting = new Greeting(1, "Hello World !");

        when(greetingService.getNextGreetingId()).thenReturn(1L);

        // When
        Greeting actualGreeting = greetingController.greeting("World");

        // Then
        assertThat(actualGreeting)
                .isNotNull()
                .usingRecursiveAssertion().isEqualTo(expectedGreeting);
    }

    @Test
    void greeting_should_return_Hello_Onepoint_with_id_3() {
        // Given
        Greeting expectedGreeting = new Greeting(3, "Hello Onepoint !");

        when(greetingService.getNextGreetingId()).thenReturn(3L);

        // When
        Greeting actualGreeting = greetingController.greeting("Onepoint");

        // Then
        assertThat(actualGreeting)
                .isNotNull()
                .usingRecursiveAssertion().isEqualTo(expectedGreeting);
    }

}