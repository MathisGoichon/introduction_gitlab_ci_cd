package com.onepoint.onetechpoint.service;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GreetingServiceTest {

    GreetingService greetingService = new GreetingService();

    @Test
    void get_next_greeting_should_return_1_if_it_called_once() {
        assertThat(greetingService.getNextGreetingId()).isEqualTo(1);
    }

    @Test
    void get_next_greeting_should_return_3_if_it_called_thrice() {
        assertThat(greetingService.getNextGreetingId()).isEqualTo(1);
        assertThat(greetingService.getNextGreetingId()).isEqualTo(2);
        assertThat(greetingService.getNextGreetingId()).isEqualTo(3);
    }
}
