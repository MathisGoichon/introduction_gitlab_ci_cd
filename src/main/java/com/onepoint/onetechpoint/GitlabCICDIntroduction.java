package com.onepoint.onetechpoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabCICDIntroduction {
    public static void main(String[] args) {
        SpringApplication.run(GitlabCICDIntroduction.class, args);
    }
}