package com.onepoint.onetechpoint.controller;

import com.onepoint.onetechpoint.model.Greeting;
import com.onepoint.onetechpoint.service.GreetingService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    private static final String template = "Hello %s !";

    GreetingService greetingService;

    public GreetingController(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    @GetMapping("/greeting")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new Greeting(greetingService.getNextGreetingId(), String.format(template, name));
    }

}
